﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetoFiap
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MensagemMenuPage : ContentPage
	{
		public MensagemMenuPage ()
		{
			InitializeComponent();
		}

        private void ButtonAssinarClicked(object s, EventArgs e)
        {
            MessagingCenter.Subscribe<String>("", "Alterar", (sender) =>
            {
                lblTexto.Text = "Alterando o label da tela";
            });

            Navigation.PopAsync();
        }

        private void ButtonCancelarClicked(object sender, EventArgs e)
        {
            MessagingCenter.Unsubscribe<String>("", "Alterar");
        }

        private void ButtonProximaClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(
                new MensagemPublicarPage()    
            );
        }
    }
}