﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjetoFiap
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MensagemPublicarPage : ContentPage
	{
		public MensagemPublicarPage ()
		{
			InitializeComponent ();
		}

        private void ButtonClicked(object sender, EventArgs e)
        {
            MessagingCenter.Send<String>("", "Alterar");
            Navigation.PopAsync();
        }
    }
}